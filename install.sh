#!/bin/sh

cwd=$(pwd)
dev=$HOME/dev
aux=$HOME/aux

[ -d "$dev" ] || mkdir -p $dev 
[ -d "$aux" ] || mkdir -p $aux 
[ -e "$dev/aux" ] || ln -s $aux $dev/aux

# Link to:
# ~/usr : Adress for scripts
# ~/dev/usr: Access in dev environment
# ~/dev/aux/usr: The aux index

for item in  $HOME $dev $aux; do
   rm -f $item/usr
   ln -s $cwd $item/usr
done


# Links to ~/dev
for dir in notepad.md dotfiles edibin ediutils; do
    rm -f $dev/$dir
    ln -s $cwd/$dir $dev/$dir
done

# Notepad
rm -f $dev/notepad.md
ln -s $cwd/notepad.md $dev/notepad.md


#for a in aliases/*; do
    #[ -f $a ] || continue
    #b=$(basename $a)
    #rm -f $HOME/.$b
    #ln -s $cwd/$a $HOME/.$b
#done
